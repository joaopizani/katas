Code Katas
==========

This is just a repo containing small "toy programs" which I've written (sometimes together with friends)
during the last years... Some people like to call this sort of programs "Code katas", so be it!")
